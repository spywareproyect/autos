/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaRecursos;

/**
 *
 * @author Jhonatan
 */
public class Vehiculos {

    private String marca;
    private String modelo;
    private String color;

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return this.marca;
    }
    
    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    
    public String getModelo(){
        return this.modelo;
    }
    
    public void setColor(String color){
        this.color=color;
    }
    
    public String getColor(){
        return this.color;
    }
}
